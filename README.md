# GitLab-Docusaurus
Prejeto incial para estudo GitLab Pipeline CI e CID

# Preparando o ambiente de laboratorio!

Essa infraestrutura foi provisonado na Oracle Cloud com a distribuição Linux CentOS 7.

Fica a Dica :+1: E possivel manter e concluir o Lab sem custo nenhum com uma conta Oracle Cloud Free Tier (https://www.oracle.com/br/cloud/free/)

VM: CentOS 7
- 1 vCPU
- 1GB ram
- Disco: 50GB


# Preparando a VM

Primeiro instale o gerenciador de pacotes que faz parte do Node.js. Esse, por sua vez, é um ambiente para a execução de JavaScript no lado do servidor de hospedagem. Em outras palavras, ele permite utilizar a linguagem JavaScript no back-end da aplicação

```
$ yum install npm
```

1. Instale o pacote epel-release.
Nota: Extra Packages for Enterprise Linux (EPEL) é um Fedora Special Interest Group que cria, mantém e gerencia pacotes adicionais para Enterprise Linux. Em outras palavras, o repositório EPEL contém as versões Fedora de pacotes adicionais que podem ser usados ​​por distribuições Enterprise Linux, incluindo CentOS.

```
$ yum install epel-release
```

2. Download Node JS v8
```
$ curl --silent --location https://rpm.nodesource.com/setup_18.x | sudo bash -
```
OU

Instale NodeJS via yum
```
$ yum install -y nodejs
```
3. Instale as ferramentas de compilação
```
$ yum install gcc-c++ make
```
4. Verifique a versão do nó e do npm executando os seguintes comandos

```
$ node -v
$ npm -v
```



# Instalação do Docusaurus

Instalação
```
$ npm install --global docusaurus-init
```
![image](https://user-images.githubusercontent.com/100965859/158063552-790cd6c5-8c09-4c50-87eb-b851e1f784fe.png)

Agora crieo diretrio que será utilizadoara para inicializar o projeto.
```
$ mkdir nomedodiretorio
```
Acesse o diretório e inicialize o Docusaurus

```
$ cd nomedodiretorio/
```
```
$ docusaurus-init OU npx docusaurus-init
```
![image](https://user-images.githubusercontent.com/100965859/158063374-4a4bae3b-4244-4a47-ab43-56836f759fc0.png)

Pronto!

# Iniciando os serviços.

Note que a estrutura do Docusaurus já foi criada e agora precisamos iniciar os serviços.
![image](https://user-images.githubusercontent.com/100965859/158063886-c1e55b6c-5b3d-42fb-be47-83beeac627ad.png)

```
$ cd website/
```

```
$ npm run start
```
![image](https://user-images.githubusercontent.com/100965859/158063941-e7730c08-d486-42b0-be02-89942c8ffcd6.png)
